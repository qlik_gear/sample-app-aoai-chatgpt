from datetime import datetime

import mysql.connector

  
class MysqlConversationClient():
    
    def __init__(self, host: str, user: str, password: str, database: str):
        self.host = host
        self.database = database
        self.user = user
        self.password = password
        
        
    def get_database_connection(self):
        return mysql.connector.connect(
            host=self.host,
            user=self.user,
            password=self.password,
            database=self.database
        )

    def mysql_get(self, id, table):
        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True)
        sql = f"SELECT * FROM {table} WHERE id = {id}"
        cursor.execute(sql)
        resp = cursor.fetchone()
        cursor.close()
        connection.close()
        return resp

    def ensure(self):
        """try:
            if not self.cosmosdb_client or not self.database_client or not self.container_client:
                return False
            
            container_info = self.container_client.read()
            if not container_info:
                return False
            
            return True
        except:
        
            return False
        """
        return True

    def create_conversation(self, user_id, user_name, title = ''):
        
        connection = self.get_database_connection()
        sql = "INSERT INTO Conversation (title, userName, userId) VALUES (%s, %s, %s)"

        print(sql)
        val = (title, user_name, user_id)
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        
        resp = self.mysql_get(cursor.lastrowid, 'Conversation')

        cursor.close()
        connection.close()
         

        if resp:
            return resp
        else:
            return False
    
    def upsert_conversation(self, conversation):

        print(f"UPDATING CONVERSTAION ID: {conversation['id']}")
        connection = self.get_database_connection()
        sql = "REPLACE INTO Conversation (id, title, userName, userId, updatedAt) VALUES (%s, %s, %s, %s, %s)"
        val = (conversation['id'], conversation['title'], conversation['userName'], conversation['userId'], conversation['updatedAt'])
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        resp = self.mysql_get(cursor.lastrowid, 'Conversation')

        connection.close()

        print(cursor.rowcount, "record(s) updated")

        cursor.close()

        if resp:
            return resp
        else:
            return False

    def delete_conversation(self, user_id, conversation_id):

        connection = self.get_database_connection()
        cursor = connection.cursor() 
        sql = f"DELETE FROM Conversation WHERE id = {conversation_id} and userId = '{user_id}'"
        cursor.execute(sql)
        connection.commit()

        print(cursor.rowcount, "record(s) deleted")

        cursor.close()
        connection.close()

        return True
        
        
    def delete_messages(self, conversation_id, user_id):
        ## get a list of all the messages in the conversation
        messages = self.get_messages(user_id, conversation_id)
        delete_list = []
        if messages:
            for message in messages:
                delete_list.append(message['id'])

            connection = self.get_database_connection()
            cursor = connection.cursor()    
            query = "DELETE FROM Message WHERE id IN (%s)" % ",".join(["%s"] * len(delete_list))
            cursor.execute(query, delete_list)
            connection.commit()
            deleted_count = cursor.rowcount

            cursor.close()
            connection.close()

            return deleted_count


    def get_conversations(self, user_id, sort_order = 'DESC'):
        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True) 
        sql = f"SELECT * FROM Conversation WHERE userId = '{user_id}' and type = 'conversation' order by updatedAt {sort_order} "
        print(f"#get_conversations: {sql}")
        cursor.execute(sql)
        conversations = cursor.fetchall()
        cursor.close()
        connection.close()

        ## if no conversations are found, return None
        if len(conversations) == 0:
            return []
        else:
            return conversations
        
    def get_message(self, message_id):

        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True) 
        sql = f"SELECT * FROM Message WHERE id = {message_id}"
        cursor.execute(sql)
        message = cursor.fetchone()
        cursor.close()
        connection.close()


        return message
                                                                           
        
    def get_conversation(self, user_id, conversation_id):
        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True) 
        sql = f"SELECT * FROM Conversation WHERE id = {conversation_id} and userId = '{user_id}'"
        cursor.execute(sql)
        conversation = cursor.fetchone()
        cursor.close()
        connection.close()

        return conversation
    
    def update_message(self, message_id, rating, feedback):

        message = self.mysql_get(message_id, 'Message')
        fb = None
        if (feedback and len(feedback)>0):
            fb = ",".join(feedback)
       
        now = datetime.now()
        connection = self.get_database_connection()
        sql = "REPLACE INTO Message (id, conversationId, rating, role, content, feedback, createdAt, updatedAt, userName, userId) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = (message_id, message['conversationId'], rating, message['role'], message['content'], fb, message['createdAt'], now, message['userName'], message['userId'])
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        resp = self.mysql_get(cursor.lastrowid, 'Message')

        connection.close()

        return resp

    def create_message(self, conversation_id, user_id, user_name, input_message: dict):
        
        print(f"Creating Message for converstaion : {conversation_id} and role = {input_message['role']}")
        connection = self.get_database_connection()
        sql = "INSERT INTO Message (conversationId, userId, userName, role, content) VALUES (%s, %s, %s, %s, %s)"
        val = (conversation_id, user_id, user_name, input_message['role'], input_message['content'])
        cursor = connection.cursor()
        cursor.execute(sql, val)
        connection.commit()
        
        resp = self.mysql_get(cursor.lastrowid, 'Message')
        
    
        if resp:
            ## update the parent conversations's updatedAt field with the current message's createdAt datetime value
            conversation = self.get_conversation(user_id, conversation_id)
            if conversation:
                conversation['updatedAt'] = resp['createdAt']
                self.upsert_conversation(conversation)
            else:
                print(f"No converstion found fo conversation_id: {conversation_id}, and user_id: {user_id} ")
                
            return resp
        else:
            return False
    


    def get_messages(self, user_id, conversation_id):
        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True) 
        sql = f"SELECT * FROM Message WHERE conversationId = {conversation_id} and userId = '{user_id}' ORDER BY createdAt ASC"
        print(f"#get_messages: {sql}")
        cursor.execute(sql)
        messages = cursor.fetchall()
        cursor.close()
        connection.close()

        return messages


    def get_messages_user(self, user_id):
        connection = self.get_database_connection()
        cursor = connection.cursor(dictionary=True) 
        sql = f"SELECT * FROM Message WHERE userId = '{user_id}' ORDER BY createdAt ASC"
        print(f"#get_messages: {sql}")
        cursor.execute(sql)
        messages = cursor.fetchall()
        cursor.close()
        connection.close()

        return messages

    

